#### Favorite posts and series

{{< cppupgrade >}} &bullet;
[macOS](/post/setup-a-new-mac) [(AS)](/post/setup-apple-silicon) / [Windows](/post/setup-windows) Setup &bullet;
[Azure&nbsp;DevOps](/categories/azure-devops) ([Python&nbsp;Wheels](/post/azure-devops-python-wheels)) &bullet;
[Conda-Forge&nbsp;ROOT](/post/root-conda) &bullet;
[CLI11](/tags/cli11) &bullet;
[GooFit](/tags/goofit) &bullet;
[cibuildwheel](/tags/cibuildwheel) &bullet;
[Hist](/tags/hist) &bullet;
[Python&nbsp;Bindings](/tags/bindings) &bullet;
{{< pyupgrade >}} &bullet;
[SSH](/post/setting-up-ssh-forwarding/)

#### My classes and books

[Modern&nbsp;CMake](https://cliutils.gitlab.io/modern-cmake/) &bullet;
[CompClass](https://henryiii.github.io/compclass) &bullet;
[se-for-sci](https://henryiii.github.io/se-for-sci)

#### My workshops

[CMake&nbsp;Workshop](https://hsf-training.github.io/hsf-training-cmake-webpage/) &bullet;
Python [CPU](https://github.com/henryiii/python-performance-minicourse), [GPU](https://github.com/henryiii/pygpu-minicourse), [Compiled](https://github.com/henryiii/python-compiled-minicourse) minicourses &bullet;
[Level&nbsp;Up Your Python](https://henryiii.github.io/level-up-your-python) &bullet;
[Packaging](https://intersect-training.org/packaging/)

#### My projects

[pybind11](https://pybind11.readthedocs.io) ([python_example](https://github.com/pybind/python_example), [cmake_example](https://github.com/pybind/cmake_example), [scikit_build_example](https://github.com/pybind/scikit_build_example)) &bullet;
[cibuildwheel](https://cibuildwheel.readthedocs.io) &bullet;
[build](https://build.pypa.io) &bullet;
[pipx](https://pipx.pypa.io) &bullet;
[nox](https://nox.thea.codes) &bullet;
[pyproject-metadata](https://github.com/pypa/pyproject-metadata) &bullet;
[scikit-build](https://github.com/scikit-build/scikit-build) ([core](https://github.com/scikit-build/scikit-build-core), [cmake](https://github.com/scikit-build/cmake-python-distributions), [ninja](https://github.com/scikit-build/ninja-python-distributions), [moderncmakedomain]()) &bullet;
[boost-histogram](https://github.com/scikit-hep/boost-histogram) &bullet;
[Hist](https://github.com/scikit-hep/hist) &bullet;
[UHI](https://github.com/scikit-hep/uhi) &bullet;
[Vector](https://github.com/scikit-hep/vector) &bullet;
[GooFit](https://github.com/GooFit/GooFit) &bullet;
[Particle](https://github.com/scikit-hep/particle) &bullet;
[DecayLanguage](https://github.com/scikit-hep/decaylanguage) &bullet;
[Conda-Forge&nbsp;ROOT](https://github.com/conda-forge/root-feedstock) &bullet;
[Jekyll-Indico](https://github.com/iris-hep/jekyll-indico) &bullet;
[uproot-browser](https://github.com/scikit-hep/uproot-browser) &bullet;
[Scientific-Python/cookie](https://github.com/scientific-python/cookie) &bullet;
[repo-review](https://github.com/scientific-python/repo-review) &bullet;
[CLI11](https://github.com/CLIUtils/CLI11) &bullet;
[meson-python](https://github.com/mesonbuild/meson-python) &bullet;
[Plumbum](https://plumbum.readthedocs.io/en/latest) &bullet;
[validate-pyproject](https://github.com/abravalheri/validate-pyproject)(-[schema-store](https://github.com/henryiii/validate-pyproject-schema-store)) &bullet;
[pytest&nbsp;GHA&nbsp;annotate-failures](https://github.com/utgwkk/pytest-github-actions-annotate-failures) &bullet;
[flake8-errmsg](https://github.com/henryiii/flake8-errmsg) &bullet;
[check-sdist](https://github.com/henryiii/check-sdist) &bullet;
[beautifulhugo](https://github.com/halogenica/beautifulhugo) &bullet;
[POVM](https://github.com/Princeton-Penn-Vents/princeton-penn-flowmeter) &bullet;
[hypernewsviewer](https://github.com/henryiii/hypernewsviewer)

#### My sites

[Scientific-Python Development Guide](https://learn.scientific-python.org/development) &bullet;
[IRIS-HEP](https://iris-hep.org) &bullet;
[Scikit-HEP](https://scikit-hep.org) &bullet;
[CLARIPHY](https://clariphy.org)
