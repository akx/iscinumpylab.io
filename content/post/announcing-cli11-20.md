---
title: "Announcing CLI11 2.0"
date: 2021-07-14T14:34:02-04:00
categories:
  - cpp
tags:
  - programming
  - cpp
  - cli
  - cli11
  - plumbum
---

[CLI11][], a powerful library for writing beautiful command line interfaces in
C++11, has [been updated to 2.0][releases]. A lot of deprecated things have been
removed, and there was a lot of cleanup under-the-hood; making CLI11 simpler. A
few defaults have changed slightly, like better TOML support by default.

CLI11 does a better job than ever understanding any sort of container you
provide - complex numbers are natively supported, along with atomic types. A
long requested feature, simple version flags, has been added. Subcommands are
more customizable. And there have been quite a few bugfixes for rare issues.

<!--more-->

## Key new features

### TOML upgrade

Introduced in 1.9, the default configuration output is now TOML compliant.
Multiline TOML is now supported, and you can control the quote character.
Short-only and positional arguments are now supported. As always, you can add
your own output formats with the extension points and class provided.

### Container improvements

`add_*` commands are better at adapting to the input you provide. The
`add_complex` method has been removed, because `add_option` now supports complex
numbers directly (this was the only removal in 2.0 that was not deprecated
beforehand). You can add atomics as well now.

Deprecated for a while, the `add_set` commands have been removed; for several
releases sets have been better described by validators (and that was used in the
backend for `add_set` anyway).

Also the deprecated `defaulted` boolean has been removed; please set a default
or use `->capture_default_str()` instead, which is far more readable then a
mysterious boolean. Also, you can set
`app.option_defaults()->always_capture_default()` to always capture the default
string for all your options (don't add an unprintable or uninitialized variable,
obviously).

{{< columns >}}

#### Before:

```cpp
using namespace std::complex_literals;

std::complex<double> v = 1i + 2;

app.add_complex("--val", v, "Help", true);
```

{{< column >}}

#### After:

```cpp
using namespace std::complex_literals;

std::complex<double> v = 1i + 2;

app.add_option("--val", v, "Help")
    ->capture_default_str();
```

{{< endcolumns >}}

### Other features

Subcommands got several new features, and some fixes. They now support aliases.
You can hide one with `->silent()`. You can now add a version flag very easily
with `.set_version_flag(...)`. And quite a few bugs have been fixed, including
avoiding a clash with `windows.h`, mixing required config files and a help flag,
and more.

### Backend changes

A lot of work on CI (heaver use of GHA and less Travis CI) and testing went into
this release. The testing framework was moved from GoogleTest / GoogleMock to
Catch2. This enabled simpler, nicer tests that can access more complex features
(like sections / parametrization) much more simply than before. We also only
used GoogleMock for matchers, which was occasionally a pain. The compiler
support for Catch2 is much better at both ends (kind-of-old and very new) than
GoogleTest.

The MakeSingleHeader script has been rewritten. It's more flexible now, with
explicit markers in the code. This will enable future work on optional
pre-compilation to happen, and will enable some parts of the library to become
optional (such as the more uncommon validators). It should still support all the
features of the original script, including the ability to generate a branded
CLI11 header.

[cli11]: https://github.com/CLIUtils/CLI11
[releases]: https://github.com/CLIUtils/CLI11/releases/tag/v2.0.0
