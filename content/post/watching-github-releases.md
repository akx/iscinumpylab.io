---
title: "Watching GitHub Releases"
date: 2017-11-28T16:54:14-05:00
categories:
  - Programming
tags:
  - programming
  - github
  - git
---

If you use an RSS feed reader, you can watch all your favorite GitHub
repositories for new releases! Just follow:

```
https://github.com/USER/REPO/releases.atom
```

You can see a long discussion with other options
[here](https://github.com/isaacs/github/issues/410#issuecomment-163761492).

<!--more-->
