---
title: Announcing CLI11 Version 1.0
date: 2017-06-01T14:05:00.000-07:00
modified-date: 2017-11-10T14:05:00.000-07:00
categories:
  - cpp
tags:
  - programming
  - cpp
  - cli
  - cli11
  - plumbum
---

CLI11, a powerful library for writing command line interfaces in C++11, has just
been released. There are no requirements beyond C++11 support (and even
`<regex>` support not required). It works on Mac, Linux, and Windows, and has
100% test coverage on all three systems. You can simply drop in a single header
file (`CLI11.hpp` available in [releases]) to use CLI11 in your own application.
Other ways to integrate it into a build system are listed in the [README].

The library was inspired the Python libraries [Plumbum] and [Click], and
incorporates many of their user friendly features. The library is extensively
documented, with a [friendly introduction][readme], a tutorial filled (in
progress) [GitBook], and more technical [API docs].

<!--more-->

The syntax is simple and scales from a basic application to a massive physics
analysis with multiple models and many parameters and switches. For example,
this is a simple program that has an optional parameter that defaults to 1:

```
./a.out
Parameter value: 0

./a.out -p 4
Parameter value: 4

./a.out --help
App description
Usage: ./a.out [OPTIONS]

Options:
  -h,--help                   Print this help message and exit
  -p INT                      Parameter
```

Like any good command line application, help is provided. This program can be
implemented in under 15 lines:

```cpp
#include "CLI11.hpp"
#include <iostream>

int main(int argc, char **argv) {
	CLI::App app{"App description"};

	// Define options
	int p = 0;
	app.add_option("-p", p, "Parameter");

	// Standard parsing lines (copy and paste in)
	try {
	    app.parse(argc, argv);
	} catch (const CLI::ParseError &e) {
	    return app.exit(e);
	}

	std::cout << "Parameter value: " << p << std::endl;
	return 0;
}
```

Unlike some other libraries, this is enough to exit correctly and cleanly if
help is requested or if incorrect arguments are passed. You can try this example
out for yourself. To compile with GCC:

```bash
g++ -std=c++11 main.cpp
```

Much more complicated options are handled elegantly:

```cpp
std::string req_real_file;
app.add_option("-f,--file", file, "Require an existing file")
  ->required()
  ->check(CLI::ExistingFile);
```

You can use any valid type; the above example could have used a
`boost::file_system` file instead of a `std::string`. The value is a real value
and does not require any special lookups to access. You do not have to risk
typos by repeating the values after parsing like some libraries require. The
library also handles positional arguments, flags, fixed or unlimited repeating
options, interdependent options, flags, custom validators, help groups, and
more.

You can use subcommands, as well. Subcommands support callback lambda functions
when parsed, or they can be checked later. You can infinitely nest subcommands,
and each is a full `App` instance, supporting everything listed above.

Reading/producing `.ini` files for configuration is also supported, as is using
environment variables as input. The base `App` can be subclassed and customized
for use in a toolkit (like [GooFit]). All the standard shell idioms, like `--`,
work as well.

CLI11 was developed at the [University of Cincinnati] to support of the [GooFit]
library under [NSF Award 1414736]. It was recently featured in a [DIANA/HEP]
meeting at CERN. Please give it a try! Feedback is always welcome.

[goofit]: https://github.com/GooFit/GooFit
[diana/hep]: http://diana-hep.org
[cli11]: https://github.com/CLIUtils/CLI11
[releases]: https://github.com/CLIUtils/CLI11/releases
[api docs]: https://cliutils.github.io/CLI11
[readme]: https://github.com/CLIUtils/CLI11/blob/master/README.md
[nsf award 1414736]: https://nsf.gov/awardsearch/showAward?AWD_ID=1414736
[university of cincinnati]: http://www.uc.edu
[plumbum]: http://plumbum.readthedocs.io/en/latest/
[click]: http://click.pocoo.org/5/
[gitbook]: https://cliutils.gitlab.io/CLI11
