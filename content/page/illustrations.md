---
title: "Illustrations and videos"
date: 2018-12-10T11:32:00+01:00
categories:
  - Python
tags:
  - programming
  - python
---

These are some illustrations and videos that I have made over the years.

<!--more-->

### University of Texas MayaMuon work

{{< figure src="diagrams/UTFastTimesFigure3.png" title="Example from FastTimes" width="640"
caption="Figure for a FastTimes article." >}}

{{< figure src="diagrams/UTDetCutawayInk.png" title="Detector Design" height="320"
caption="An illustration of our detector strips." >}}

{{< figure src="diagrams/UTMayaDetGUI2.png" title="Icon for DetGUI2" width="128"
caption="The icon for DetGUI 2, which could run our detectors." >}}

{{< figure src="diagrams/ThesisSteelStack.png" title="Stack of steel plates" height="320"
caption="Discarded concept art for a momentum measurement." >}}

{{< figure src="diagrams/ThesisSimpleHanger.png" title="Suspension system for detector casing" height="320"
caption="Concept art for the actual mechanism used" >}}

### University of Texas Lab 102m rewrite

All 11 labs were recorded and you can see them on YouTube
[here](https://www.youtube.com/playlist?list=PL_WfVE_T0ipZJ9aptpIEATzSArE8Lg_DV).
Here is the list:

- [Lab 1: Kinematics](https://www.youtube.com/watch?v=9C1hk7UNSdE) - the first
  lab I was just getting used to making the videos, and hadn't perfected
  recording yet.
- [Lab 2: Vectors](https://www.youtube.com/watch?v=LfI7qzWbgpY) - This one is
  not my best speaking performance, but has some useful animations.
- [Lab 3: Atwood machine, forces](https://www.youtube.com/watch?v=a34ty_GMt2U)
- [Lab 4: Atwood machine, energy](https://www.youtube.com/watch?v=i1aFY3qy6R0)
- [Lab 5: Collisions](https://www.youtube.com/watch?v=IBlZKBM4FQQ) - I really
  like the animations here.
- [Lab 6: Torque](https://www.youtube.com/watch?v=iySZKw2B-HA) - I which I
  didn't draw the arrow backward for the torque. I still like this one though.
- [Lab 7: Rotational motion](https://www.youtube.com/watch?v=bS6KLyhZ4YI)
- [Lab 8: Buoyancy](https://www.youtube.com/watch?v=fy-LJ7EH4Kc) - Another one
  with nice animations
- [Lab 9: Pendulums and springs](https://www.youtube.com/watch?v=bNOel6qdqbg) -
  I also like the animations here.
- [Lab 10: Standing waves](https://www.youtube.com/watch?v=jP7hHxKOuwI) - The
  final two labs were filmed on a phone the semester before, and were the test
  pilot for the videos.
- [Lab 11: Heat](https://www.youtube.com/watch?v=arvtFAjgLyo)

All new consistently styled figures were created for my rewrite of the lab
manual for the class I was head TA for. Figures were primarily designed in
Blender with a shared theme and an automatic renderer that could produce all
figures at once from sources. A few TiKZ figures and Inkscape figures were
included, as well, when appropriate. The TiKZ and Inkscape Verneer caliper
figure was written as a LaTeX function, with opening amount as a parameter, and
optional explanations, and was also used in tests. Here is a sampling of
figures. Some figures were featured as animations in the video series, with new
styling to match the video theme. One image chosen from each of the 11 labs.

{{< figure src="diagrams/LabManChap1.png" title="Diagram from lab 1" width="320" >}}

{{< figure src="diagrams/LabManChap2.png" title="Diagram from lab 2" width="320" >}}

{{< figure src="diagrams/LabManChap3.png" title="Diagram from lab 3" width="320" >}}

{{< figure src="diagrams/LabManChap5.png" title="Diagram from lab 5" width="480" >}}

{{< figure src="diagrams/LabManChap6.png" title="Diagram from lab 6" width="320" >}}

{{< figure src="diagrams/LabManChap7.png" title="Diagram from lab 7" width="320" >}}

{{< figure src="diagrams/LabManChap8.png" title="Diagram from lab 8" width="320" >}}

{{< figure src="diagrams/LabManChap9.png" title="Diagram from lab 9" height="320" >}}

{{< figure src="diagrams/LabManChap10.png" title="Diagram from lab 10" width="320" >}}

{{< figure src="diagrams/LabManChap11.png" title="Diagram from lab 11" height="320" >}}

### University of Cincinnati images

{{< figure src="diagrams/CinciPyBindings.png" title="Python bindings." width="480"
caption="Python bindings timeline." >}}

{{< figure src="diagrams/CinciGooFitSnake.png" title="GooFit 2.1+." width="480"
caption="Python bindings in GooFit." >}}

{{< figure src="diagrams/CinciShipload.png" title="A ship load of charm." width="320"
caption="Illustration of Run 2 vs. Run 3." >}}

### Angelo State University Modern lab manual rewrite

I helped with the modern lab rewrite as an undergraduate, including supplying
many of the figures. May add them here someday if I find them.

### Other (personal)

Some images:

- [Martia Lisa](https://www.deviantart.com/hsiii/art/The-Martia-Lisa-79931994) -
  A project for an introductory art class as an undergraduate.
- [Rosco plays racquetball](https://www.deviantart.com/hsiii/art/Rosco-Racketball-79932761) -
  Old image for a Racquetball club. I made a better version at one point that
  had a white background that was used as a poster.
- [Cave dragon](https://www.deviantart.com/hsiii/art/Cave-Dragon-80531322) - A
  dragon in a cave.

And, some videos:

- [Live wavelet encoding](https://www.youtube.com/watch?v=0VnzlVhcBuA) - Used in
  a talk to demonstrate wavelet encoding on a video. Used a clip from Brothers
  of the Force.
- [Brothers of the force](https://www.youtube.com/watch?v=QkrV-5Re724) - Two
  brothers find something incredible. A homemade Star Wars inspired mini-film
  that I made as an undergraduate with my family.
