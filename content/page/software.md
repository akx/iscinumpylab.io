---
title: "My Software"
date: 2017-12-06T13:17:33-05:00
---

## My projects

Besides being an admin at [Scikit-HEP][], and active in conda-forge and
homebrew, these are some of the projects I work on directly:

| Name                                        | Description                                                              |
| ------------------------------------------- | ------------------------------------------------------------------------ |
| [pybind11][]                                | Binding C++11+ and Python beautifully.                                   |
| [cibuildwheel][]                            | Create Python wheels beautifully.                                        |
| [boost-histogram][]                         | Histograms in Python based on Boost.Histogram for C++14.                 |
| [CLI11][]                                   | A powerful but easy to use C++11 command line interface parser.          |
| [Particle][]                                | Particle descriptions in Python.                                         |
| [DecayLanguage][]                           | Decay chains in Python.                                                  |
| [Plumbum][]                                 | A shell tools library for Python, with color, ssh, paths, cli, and more. |
| [GooFit][]                                  | An OpenMP/CUDA powered fitting library.                                  |
| [Jeykyll-Indico][]                          | A plugin to collect Indico meetings for Jekyll, written in Ruby.         |
| [Conda-Forge ROOT][]                        | A project that did the impossible: make ROOT a conda package!            |
| [pytest-github-actions-annotate-failures][] | A utility to convert pytest errors to annotations in GitHub Actions.     |

[jekyll-indico]: https://github.com/iris-hep/jekyll-indico

Classic or smaller historic projects:

| Name                | Description                                                                   |
| ------------------- | ----------------------------------------------------------------------------- |
| [GitBook term][]    | A plugin to add nice terminals to GitBook.                                    |
| [CLIUtils/cmake][]  | A collection of tools for CMake in science.                                   |
| [Envmodule setup][] | Examples for setting up systems with Lmod.                                    |
| [HomeBrew libomp][] | A way to add OpenMP to macOS default compiler (now integrated into brew-core) |

[pybind11]: https://pybind11.readthedocs.io
[cibuildwheel]: https://cibuildwheel.readthedocs.io
[conda-forge root]: https://github.com/conda-forge/root-feedstock
[scikit-hep]: https://scikit-hep.org
[particle]: https://github.com/scikit-hep/particle
[decaylanguage]: https://github.com/scikit-hep/decaylanguage
[boost-histogram]: https://github.com/scikit-hep/boost-histogram
[cli11]: https://github.com/CLIUtils/CLI11
[goofit]: https://github.com/GooFit/GooFit
[plumbum]: https://plumbum.readthedocs.io/en/latest
[gitbook term]: https://github.com/CLIUtils/gitbook-plugin-term
[cliutils/cmake]: https://github.com/CLIUtils/cmake
[envmodule setup]: https://github.com/CLIUtils/envmodule_setup
[homebrew libomp]: https://github.com/CLIUtils/homebrew-apple
[pytest-github-actions-annotate-failures]:
  https://github.com/utgwkk/pytest-github-actions-annotate-failures

## Contributions

This is a very incomplete list of projects I contribute to, at least
occasionally:

| Name         | Description                                                           |
| ------------ | --------------------------------------------------------------------- |
| [Numba][]    | A JIT compiler for Python.                                            |
| [Hydra][]    | A templated framework for HEP OpenMP/CUDA execution.                  |
| [IPython][]  | An interactive shell for Python.                                      |
| [CMake][]    | An amazing build system (if you use a modern version of it).          |
| [Eigen][]    | A matrix library for C++ that supports CUDA.                          |
| [pybind11][] | An amazing C++11 Python binding tool.                                 |
| [VexCL][]    | Computations on many backends.                                        |
| [Lmod][]     | A tool to make environments easy to use, for supercomputers and more. |

[hydra]: https://github.com/MultithreadCorner/Hydra/tree/develop
[ipython]: https://github.com/ipython/ipython
[numba]: https://github.com/numba/numba
[cmake]: https://cmake.org
[eigen]: http://eigen.tuxfamily.org
[scikit-hep]: https://github.com/scikit-hep/scikit-hep
[pybind11]: https://github.com/pybind/pybind11
[vexcl]: https://github.com/ddemidov/vexcl
[lmod]: https://github.com/TACC/Lmod
[rootpy]: https://github.com/rootpy/rootpy

## Interesting projects

These are some of my favorite software packages and libraries.

| Name      | Description                |
| --------- | -------------------------- |
| [Blender] | 3D modeler and a lot more. |

[blender]: https://blender.org
