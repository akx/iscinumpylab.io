---
title: "Writing"
date: 2017-11-10T22:08:21-05:00
---

## Git/Jupyter Books

Here is a list of GitBooks that I have either written or helped write:

| Name                      | Description                                                                              |
| ------------------------- | ---------------------------------------------------------------------------------------- |
| [Level Up Your Python][]  | Intermediate to advanced Python, for a Princeton Research Computing Workshop.            |
| [Modern CMake][]          | A fantastic, up-to-date resource for CMake the way it should be.                         |
| [CompClass][]             | Computational Science in Python course given at the University of Cincinnati, Fall 2018. |
| [DevelopKit][]            | Developing software in LHCb's Run 3.                                                     |
| [GitBook Plugin - Term][] | A powerful terminal formatting plugin (used in other GitBooks).                          |
| [CLI11 Tutorial][]        | Command line parsing made beautiful.                                                     |
| [UC ROOT Tutorial][]      | Basic ROOT for HEP, conversion from old material.                                        |
| [GooFit 2Torial][]        | Using GooFit, writing GooFit.                                                            |

[level up your python]: https://henryiii.github.io/level-up-your-python
[developkit]: https://lhcb.github.io/developkit-lessons/first-development-steps/
[modern cmake]: https://cliutils.gitlab.io/modern-cmake/
[compclass]: https://github.com/henryiii/compclass
[gitbook plugin - term]: https://cliutils.gitlab.io/plugin-term
[cli11 tutorial]: https://cliutils.github.io/CLI11/book/
[uc root tutorial]: https://goofit.gitlab.io/root-tutorial
[goofit 2torial]: https://goofit.gitlab.io/Goo2Torial

## Tutorials and workshops

| Name                                                                                    | Description                                      |
| --------------------------------------------------------------------------------------- | ------------------------------------------------ |
| [HSF Modern CMake workshop](https://hsf-training.github.io/hsf-training-cmake-webpage/) | Workshop on CMake for ATLAS students at LBNL.    |
| [Python CPU minicourse](https://github.com/henryiii/python-performance-minicourse)      | Minicourse for high-performance CPU programming. |
| [Python GPU minicourse](https://github.com/henryiii/pygpu-minicourse)                   | Minicourse for GPU programming.                  |
| [Pandas demo](https://github.com/henryiii/pandas-notebook)                              | Demo of Pandas.                                  |

## Websites

These are websites I have either created or worked on.

| Name                                     | About                                                                                   | Platform |
| ---------------------------------------- | --------------------------------------------------------------------------------------- | -------- |
| [CLARIPHY][]                             | Artificial Intelligence research to enable discoveries in particle physics.             | Jekyll   |
| [Science Responds][]                     | Information source hub for COVID-19 related research, resources, and research projects. | Jekyll   |
| [IRIS-HEP][]                             | Institute for sustainable software site. Includes Ruby plugins.                         | Jekyll   |
| [Scikit-HEP][]                           | Org for Python in HEP.                                                                  | Jekyll   |
| [ISciNumPy][]                            | My blog over programming.                                                               | Hugo     |
| [UC Henry][]                             | My UC centric blog and site.                                                            | Hugo     |
| [SSE ML LHCb][]                          | Written in Hugo.                                                                        | Hugo     |
| [GooFit][]                               | Written in Jekyll.                                                                      | Jekyll   |
| [MayaMuon][]                             | Written in Nikola.                                                                      | Nikola   |
| [PHY102M][]                              | Pure HTML.                                                                              | HTML     |
| [Angelo State HSA][]                     | My site has been replaced, but the logo is still my design.                             | HTML     |
| Angelo State Society of Physics Students | No longer up.                                                                           | Drupal   |

[clariphy]: https://clariphy.org
[science responds]: https://science-responds.org
[iris-hep]: https://iris-hep.org
[scikit-hep]: https://scikit-hep.org
[iscinumpy]: https://iscinumpy.dev
[uc henry]: https://hschrein.web.cern.ch/hschrein/
[sse ml lhcb]: https://sse-ml-lhcb.gitlab.io
[goofit]: https://goofit.github.io
[mayamuon]: http://www.hep.utexas.edu/mayamuon/aboutus.html
[phy102m]: https://web2.ph.utexas.edu/~phy102m/
[angelo state hsa]: http://www.angelo.edu/org/hsa/
