---
title: "Useful Links"
date: 2021-10-22T15:44:45-04:00
---

# Python

## Learning materials

- [HSF training curriculum (Python, bash, git, docker, and more)](https://hepsoftwarefoundation.org/training/curriculum.html)

## Topical articles

### CPython core developers

**Brett Cannon** is a CPython core developer, and is very interested in
standardizing packaging. <https://snarky.ca>

- [Why I don't like SemVer anymore](https://snarky.ca/why-i-dont-like-semver/):
  A great resource to make you think about what SemVer means and where it falls
  short.
- [What the heck is pyproject.toml](https://snarky.ca/what-the-heck-is-pyproject-toml/):
  A intro to modern Python packaging
- [A quick-and-dirty guide on how to install packages for Python](https://snarky.ca/a-quick-and-dirty-guide-on-how-to-install-packages-for-python/):
  This showw the "correct" way to throw together packages (pre-pipx).

**Paul Ganssle** is CPython core developer, and maintains zoneinfo.
<https://blog.ganssle.io>

- [Why you shouldn't invoke setup.py directly](https://blog.ganssle.io/articles/2021/10/setup-py-deprecated.html):
  The definitive resource for avoiding `python setup.py <command>`.

**Hynek Schlawack** also maintains Twisted, Attrs, and lots of things. He's also
interested in DevOps. <https://hynek.me>

- [Semantic Versioning Will Not Save You](https://hynek.me/articles/semver-will-not-save-you/):
  You can deduce from the title.
- [https://hynek.me/articles/testing-packaging/](https://hynek.me/articles/testing-packaging/):
  A description of why `/src` structure is so important, with real life
  examples.
- [Subclassing in Python Redux](https://hynek.me/articles/python-subclassing-redux/):
  A huge and fantastic article. While I love subclassing, and I think this might
  scare younger readers off of the usefulness of subclassing just a bit, it has
  great design points that are important, even if you subclass (and yes, there
  are times when that's best, and the article does point that out).

### Random sources

The [Real Python](https://realpython.com) articles are great. Tutorials are
paid, but the articles are free.

## Podcasts

There are some great podcasts for Python, such as:

- [Python Bytes](https://pythonbytes.fm): Audio Python headlines every week,
  interesting articles and things that pop up, with a random special guest.
- [Talk Python to Me](https://talkpython.fm/home): In-depth interviews. Might I
  recommend
  [episode #338](https://talkpython.fm/episodes/show/338/using-cibuildwheel-to-manage-the-scikit-hep-packages)?
- [Test and Code](https://testandcode.com): Tests are very important, and here's
  a whole podcast dedicated to them!

# C++

# CMake
